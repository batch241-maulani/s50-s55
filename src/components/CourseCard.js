import {useState, useEffect} from 'react'

import {Link} from 'react-router-dom'

import {Card,Row,Col,Button} from 'react-bootstrap'




export default function CourseCard({course}) {

	const {name, description, price, _id} = course
/*
	const [count,setCount] = useState(0)

	const [seats,setSeat] = useState(30)

	const [isOpen, setIsOpen] = useState(true)

	function enroll() {
		if(seats>0)
		{
			setCount(count + 1)
			setSeat(seats - 1)
		}else{
			alert("No more Seats Available")
		}
	}

	useEffect(()=>{
		if(seats===0){
			setIsOpen(false)
			document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled',true)
		}
	},[seats])*/




	console.log(useState)

	return (
		<Row className="mt-3 mb-3">
		<Col xs={12} md={12}>
		<Card className="cardHighlight p-3">
		<Card.Body>
		<Card.Title>
		<h2>{name}</h2>
		</Card.Title>
		<Card.Subtitle>
		<strong>Description:</strong>
		</Card.Subtitle>
		<Card.Text>
		{description}
		</Card.Text>
		<Card.Subtitle>
		Price
		</Card.Subtitle>
		<Card.Text>
		PHP {price}
		</Card.Text>
		{/*<Card.Text>
		Enrollees: {count}
		</Card.Text>
		<Card.Text>
		Available Seats: {seats}
		</Card.Text>
		<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
		<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		</Card.Body>
		</Card>
		</Col>
		</Row>
		)
}