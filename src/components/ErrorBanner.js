import {Row,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'



export default function ErrorBanner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Page Not Found</h1>
            <p>Go Back to <a href="/" as={Link}> homepage </a></p>
           
        </Col>
    </Row>
	)
}