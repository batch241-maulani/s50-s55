
import {useState,useEffect} from 'react'

import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'


export default function Courses() {

	const [courses,setCourses] = useState([])

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/allactive`).then(res => res.json()).then(data=> {
			console.log(data)
			setCourses(data.map(course=>{
				return(
					<CourseCard key={course._id} course =  {course}/>
				)
			}))
		})
	},[])

	/*const courses = coursesData.map(course => {
		return (
				
			)
	})
*/

	return (
		<>
		{courses}	
		</>
	)



}






